## Change of support system

**Due to a [change in Gitlab support desk](https://about.gitlab.com/blog/2022/03/24/efficient-free-tier/), we are moving to a [new support platform](https://support.bitergia.com) (this Gitlab support repository will be closed the 1st of September 2022).**


We want to update you on some support news and changes in the ticketing system that will improve the communication with us: you could send an email to a support account email address, and all your workmates could participate in the tickets of your company or community.

1. A new email address (support@bitergia.com) to contact support and open tickets is already available.
1. A new portal ([support.bitergia.com](support.bitergia.com)) with a form to open tickets is already available.

### New email address to contact support and to open tickets 

We would like to improve your communication with us as much as possible, that is why we have thought that a support email will make your life easier. 

When you send us en email, a new ticket will be created automatically in the support portal, where you will be able to follow it.

The new email address is support@bitergia.com

### New support portal

New support portal platform URL: [support.bitergia.com](https://support.bitergia.com/
)

You will be able to submit a ticket as well in the following portal. Only the registered users will be able to see and reply to the company’s tickets. To Bitergia, the main contact of the company is able to add a new account just opening a ticket if you want to include a new person in the support platform.

### What is going to happen with the tickets already opened in the GitLab Support tracker?:

The open tickets will be migrated to the new platform if they are still open after 1st September 2022

Please, let us know if there is any old ticket you would like to migrate as well.
